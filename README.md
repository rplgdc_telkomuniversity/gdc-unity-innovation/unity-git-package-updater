# Unity Git Package Updater
Easily update Unity packages hosted via git

![](https://gitlab.com/rplgdc_telkomuniversity/gdc-unity-innovation/unity-git-package-updater/uploads/5872b751cacfbfbcdb0d5168053f4d80/image.png)

## Feature
- Show Existing Git Package
- Check Update from Git Url
- Update all git package
- Update all package
- Update single package


## Location
GDC/Git Package Updater

## Instalation
Please check documentation below
> https://docs.unity3d.com/2019.3/Documentation/Manual/upm-ui-giturl.html

## Contributor


| Profile | Profile | 
| ------ | ------ |
| [![Firdiar](https://gitlab.com/uploads/-/system/user/avatar/2307294/avatar.png?raw=true)](https://www.linkedin.com/in/firdiar) | [![QuantumCalzone](https://avatars.githubusercontent.com/u/868794)](https://github.com/QuantumCalzone) |
| [Firdiansyah Ramadhan](https://www.linkedin.com/in/firdiar) | [QuantumCalzone](https://github.com/QuantumCalzone) | 

## License

MIT

**Free Software, Hell Yeah!**
